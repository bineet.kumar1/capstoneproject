package com.springboot.service;

import java.util.List;

import com.springboot.model.Customer;

public interface CustomerService {
	List<Customer> getAllCustomers();
	void saveCustomer(Customer customer);
}
